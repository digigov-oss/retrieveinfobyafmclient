# retrieveInfoByAFMClient

Client to connect on GSIS service.

Returns Natural and Non-Natural Person data based on the A.F.M.

#### Example

```
import getInfo from '@digigov-oss/retreive-info-by-afm-client';
import config from './config.json'; 

const test = async () => {
const input = {
    afm: "979701876"
   }

try {
        const Info = await getInfo(input, config.user, config.pass);
        return Info; 
    } catch (error) {
        console.log(error);
    }
test().then((record) => { console.log(record.retrieveInfoByAFMRecord.basicInfo); });
}
```
* you can use `overrides` to override the default values for auditrecord and input fields.
* for your tests, you don't need to use the `overrides` mechanism, in that case, the default storage path will be used ie `/tmp`
* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine. Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

#### Returns
an object like the following:
```
{
        retrieveInfoByAFMRecord: {
                basicInfo: {
                        afm: '979701876',
                        doy: '1209',
                        doyDescr: 'Δ΄ ΠΕΙΡΑΙΑ(ΣΤ΄,Δ΄ ΠΕΙΡΑΙΑ)',
                        iNiFlag: '2',
                        deactivationFlag: '1',
                        onomasia: 'ΔΥΟ ΕΝΑ',
                        firmDoy: '1209',
                        firmAddress: 'ΧΙΟΥ',
                        firmAddressNo: '18',
                        firmZipCode: '18122',
                        firmParDescr: 'ΚΟΡΥΔΑΛΛΟΣ'
                },
                firmActivitiesArray: null,
                membershipsArray: null,
                relationsArray: null,
                symmetoxesArray: null,
                sysxetiseisArray: null,
                branchArray: null,
                branchActivitiesArray: null,
                branchAbroadArray: null,
                firmAbroad: null
        },
        callSequenceId: 49080246,
        callSequenceDate: Thu May 05 2022 15:03:07 GMT+0300 (Θερινή ώρα Ανατολικής Ευρώπης),
        errorRecord: null,
        auditUnit: 'gov.gr',
        auditTransactionId: '50',
        auditProtocol: '50/2022-05-05',
        auditTransactionDate: '2022-05-05T12:03:07Z',
        auditUserIp: '127.0.0.1',
        auditUserId: 'system'
}
```
or an error object like the following:
```
{
        retrieveInfoByAFMRecord: {
                basicInfo: null,
                firmActivitiesArray: null,
                membershipsArray: null,
                relationsArray: null,
                symmetoxesArray: null,
                sysxetiseisArray: null,
                branchArray: null,
                branchActivitiesArray: null,
                branchAbroadArray: null,
                firmAbroad: null
        },
        callSequenceId: 49080247,
        callSequenceDate: Thu May 05 2022 15:03:08 GMT+0300 (Θερινή ώρα Ανατολικής Ευρώπης),
        errorRecord: {
                errorCode: 'RG_WS_ALL_DATA_AFM_FIRM_ACT_ACCESS_PROHIBITED',
                errorDescr: 'Δεν έχετε δικαίωμα πρόσβασης στην εξής υποενότητα στοιχείων μητρώου: ΔΡΑΣΤΗΡΙΟΤΗΤΕΣ ΕΠΙΧΕΙΡΗΣΗΣ.'
        },
        auditUnit: 'gov.gr',
        auditTransactionId: '51',
        auditProtocol: '51/2022-05-05',
        auditTransactionDate: '2022-05-05T12:03:07Z',
        auditUserIp: '127.0.0.1',
        auditUserId: 'system'
}
```
### Notes
On test server there not an AFM you can use to test the firm! (even the one the documentation use on examples!).
So if you set `firmActivitiesRequest` and other booleans you are always getting empty results.
In case of KED advertises wrong endpoint on production you have to use (override) the endpoint: `https://ked.gsis.gr/esb/registryService`
You can do that by setting the `endpoint` property on the `overrides` object.
```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/registryService',
}
```