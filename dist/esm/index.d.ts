import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
export type BasicInfoRecord = {
    afm: string;
    doy: string;
    doyDescr: string;
    iNiFlag: '1' | '2';
    deactivationFlag: '1' | '2';
    assTxpActual?: string;
    firmFlag?: '1' | '2' | '3';
    onomasia: string;
    mothersFirstName?: string;
    ninLegalPurpose?: string;
    ninLegalPurposeDescr?: string;
    ninLegalStatus?: string;
    ninLegalStatusDescr?: string;
    cardNo?: string;
    cardKind?: string;
    cntCitizenshipDescr?: string;
    birthDate?: string;
    birthPlace?: string;
    deathDate?: string;
    indPhone?: string;
    postalAddress?: string;
    postalAddressNo?: string;
    postalZipCode?: string;
    postalParDescr?: string;
    cntResidenceDescr: string;
    residenceAddress: string;
    residenceAddressNo: string;
    residenceZipCode: string;
    residenceParDescr: string;
    firmCommerTitle?: string;
    firmDoy?: string;
    firmAddress?: string;
    firmAddressNo?: string;
    firmZipCode?: string;
    firmParDescr?: string;
    firmPhone?: string;
    firmFax?: string;
    registDate?: string;
    stopDate?: string;
    facMainActivity?: string;
    actMainDescr?: string;
    countOfBranches?: string;
    frmForOriginDescr?: string;
    frmBooks?: string;
    frmBooksDescr?: string;
    frmVatStatus?: string;
    frmVatStatusDescr?: string;
    frmFstStateDescr?: string;
    ninFPMState?: string;
};
export type FirmActivitiesRecord = {
    firmActCode: string;
    firmActDescr: string;
    firmActKind: '1' | '2' | '3' | '4';
    firmActStartDate: string;
};
export type MembershipRecord = {
    memberParticipAfm: string;
    memberOnomasia: string;
    memberKind: '1' | '2' | '3';
    memberStartDate: string;
    memberStopDate: string;
};
export type RelationRecord = {
    relRelatedAfm: string;
    relRelatedOnomasia: string;
    relCateg: string;
    relCategDescr: string;
    relKind: string;
    relKindDescr: string;
    relStartDate: string;
    relStopRelationDate: string;
};
export type SymmetoxesRecord = {
    symmetoxesAfm: string;
    symmetoxesOnomasia: string;
    symmetoxesKind: '1' | '2' | '3';
    symmetoxesStartDate: string;
    symmetoxesStopDate: string;
};
export type SysxRecord = {
    sysxAfm: string;
    sysxOnomasia: string;
    sysxCateg: string;
    sysxCategDescr: string;
    sysxKind: string;
    sysxKindDescr: string;
    sysxStartDate: string;
    sysxStopRelationDate: string;
};
export type BranchRecord = {
    brnUnitNumber: string;
    brnUnkKindDescr: string;
    brnAddress: string;
    brnAddressNo: string;
    brnZipCode: string;
    brnParDescr: string;
    brnUnitCommerTitle: string;
    brnStartDate: string;
    brnUnitPhone: string;
    brnUnitFax: string;
    brnDolDoyDoy: string;
};
export type BranchAbrRecord = {
    bacUnitNumber: string;
    bacActCode: string;
    bacActDescr: string;
    bacKind: '1' | '2' | '3' | '4';
    bacStartDate: string;
};
export type FirmAbroadRecord = {
    fabAfmAbroad: string;
    fabAppelationAbroad: string;
    fabAddressAbroad: string;
    fabCountry: string;
    fabStartDate: string;
    fabObjective: string;
    fabTaxAuthority: string;
    fabActCode: string;
    fabActDescr: string;
    code: string;
    description: string;
    callSequenceId: string;
    callSequenceDate: string;
};
export type RetrieveInfoByAFMRecordOutput = {
    basicInfo: BasicInfoRecord | null;
    firmActivitiesArray: FirmActivitiesRecord[] | null;
    membershipsArray: MembershipRecord[] | null;
    relationsArray: RelationRecord[] | null;
    symmetoxesArray: SymmetoxesRecord[] | null;
    sysxetiseisArray: SysxRecord[] | null;
    branchArray: BranchRecord[] | null;
    branchActivitiesArray: BranchAbrRecord[] | null;
    firmAbroad: FirmAbroadRecord | null;
};
export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
export type RetrieveInfoByAFMInputRecord = {
    afm: string;
    firmActivitiesRequest?: boolean;
    membershipsRequest?: boolean;
    relationsRequest?: boolean;
    symmetoxesRequest?: boolean;
    sysxetiseisRequest?: boolean;
    branchActivitiesRequest?: boolean;
    branchAbroadRequest?: boolean;
    firmAbroadRequest?: boolean;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param input retrieveInfoByAFMInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export declare const getInfo: (input: RetrieveInfoByAFMInputRecord, user: string, pass: string, overrides?: Overrides) => Promise<{
    kedResponse: ErrorRecord | RetrieveInfoByAFMRecordOutput;
    auditRecord: AuditRecord;
}>;
export default getInfo;
