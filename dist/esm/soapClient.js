import thesoap from 'soap';
let soap = thesoap;
try {
    soap = require('soap');
}
catch (error) {
    //my hackish way to make soap work on both esm and cjs
    //theshoap on esm is undefined
    //On esm require is not defined, however on cjs require can be used.
    //So we try to use require and if it fails we use the thesoap module
}
/**
 * SOAP client for retrieveInfoByAFM
 *
 * @class Soap
 * @description SOAP client for retrieveInfoByAFM
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
class Soap {
    _wsdl;
    _username;
    _password;
    _auditRecord;
    _endpoint;
    constructor(wsdl, username, password, auditRecord, endpoint) {
        this._wsdl = wsdl;
        this._username = username;
        this._password = password;
        this._auditRecord = auditRecord;
        this._endpoint = endpoint;
    }
    async init() {
        try {
            const client = await soap.createClientAsync(this._wsdl, {
                wsdl_headers: {
                    Authorization: 'Basic ' +
                        Buffer.from(`${this._username}:${this._password}`).toString('base64'),
                },
            });
            if (this._endpoint) {
                client.setEndpoint(this._endpoint);
            }
            return client;
        }
        catch (e) {
            throw e;
        }
    }
    async getInfo(input) {
        const client = await this.init();
        var options = {
            hasNonce: true,
            actor: 'actor',
        };
        var wsSecurity = new soap.WSSecurity(this._username, this._password, options);
        client.setSecurity(wsSecurity);
        const auditRecord = this._auditRecord;
        const normalizedInput = {
            afm: input.afm,
            firmActivitiesRequest: input.firmActivitiesRequest ? 1 : 0 ?? 0,
            membershipsRequest: input.membershipsRequest ? 1 : 0 ?? 0,
            relationsRequest: input.relationsRequest ? 1 : 0 ?? 0,
            symmetoxesRequest: input.symmetoxesRequest ? 1 : 0 ?? 0,
            sysxetiseisRequest: input.sysxetiseisRequest ? 1 : 0 ?? 0,
            branchActivitiesRequest: input.branchActivitiesRequest ? 1 : 0 ?? 0,
            branchAbroadRequest: input.branchAbroadRequest ? 1 : 0 ?? 0,
            firmAbroadRequest: input.firmAbroadRequest ? 1 : 0 ?? 0,
        };
        const args = {
            auditRecord: auditRecord,
            retrieveInfoByAFMRecord: normalizedInput,
        };
        const result = await client.retrieveInfoByAFMAsync(args);
        const errorRecord = result[0].errorRecord;
        if (errorRecord) {
            return errorRecord;
        }
        else {
            return result[0]
                .retrieveInfoByAFMRecord;
        }
    }
}
export default Soap;
