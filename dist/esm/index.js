import SoapClient from './soapClient.js';
import { generateAuditRecord, FileEngine, } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json';
/**
 *
 * @param input retrieveInfoByAFMInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export const getInfo = async (input, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    const s = new SoapClient(wsdl, user, pass, auditRecord, endpoint);
    const response = await s.getInfo(input);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};
export default getInfo;
