import SoapClient from './soapClient.js';
import {
    generateAuditRecord,
    AuditRecord,
    FileEngine,
    AuditEngine,
} from '@digigov-oss/gsis-audit-record-db';
import config from './config.json';

export type BasicInfoRecord = {
    afm: string;
    doy: string;
    doyDescr: string;
    iNiFlag: '1' | '2'; // individual or company
    deactivationFlag: '1' | '2'; // active or deactivated
    assTxpActual?: string;
    firmFlag?: '1' | '2' | '3'; // 1 = Tradesman, 2 = Non-Tradesman, 3 = Former Tradesman
    onomasia: string;
    mothersFirstName?: string;
    ninLegalPurpose?: string;
    ninLegalPurposeDescr?: string;
    ninLegalStatus?: string;
    ninLegalStatusDescr?: string;
    cardNo?: string; //ADT
    cardKind?: string;
    cntCitizenshipDescr?: string;
    birthDate?: string;
    birthPlace?: string;
    deathDate?: string;
    indPhone?: string; //landline phone
    postalAddress?: string;
    postalAddressNo?: string;
    postalZipCode?: string;
    postalParDescr?: string;
    cntResidenceDescr: string;
    residenceAddress: string;
    residenceAddressNo: string;
    residenceZipCode: string;
    residenceParDescr: string;
    firmCommerTitle?: string;
    firmDoy?: string;
    firmAddress?: string;
    firmAddressNo?: string;
    firmZipCode?: string;
    firmParDescr?: string;
    firmPhone?: string;
    firmFax?: string;
    registDate?: string;
    stopDate?: string;
    facMainActivity?: string;
    actMainDescr?: string;
    countOfBranches?: string;
    frmForOriginDescr?: string;
    frmBooks?: string;
    frmBooksDescr?: string;
    frmVatStatus?: string;
    frmVatStatusDescr?: string;
    frmFstStateDescr?: string;
    ninFPMState?: string;
};

export type FirmActivitiesRecord = {
    firmActCode: string;
    firmActDescr: string;
    firmActKind: '1' | '2' | '3' | '4'; //1 = main, 2 = Secondary, 3 other, 4 = auxiliary
    firmActStartDate: string;
};

export type MembershipRecord = {
    memberParticipAfm: string;
    memberOnomasia: string;
    memberKind: '1' | '2' | '3';
    memberStartDate: string;
    memberStopDate: string;
};

export type RelationRecord = {
    relRelatedAfm: string;
    relRelatedOnomasia: string;
    relCateg: string;
    relCategDescr: string;
    relKind: string;
    relKindDescr: string;
    relStartDate: string;
    relStopRelationDate: string;
};

export type SymmetoxesRecord = {
    symmetoxesAfm: string;
    symmetoxesOnomasia: string;
    symmetoxesKind: '1' | '2' | '3';
    symmetoxesStartDate: string;
    symmetoxesStopDate: string;
};

export type SysxRecord = {
    sysxAfm: string;
    sysxOnomasia: string;
    sysxCateg: string;
    sysxCategDescr: string;
    sysxKind: string;
    sysxKindDescr: string;
    sysxStartDate: string;
    sysxStopRelationDate: string;
};

export type BranchRecord = {
    brnUnitNumber: string;
    brnUnkKindDescr: string;
    brnAddress: string;
    brnAddressNo: string;
    brnZipCode: string;
    brnParDescr: string;
    brnUnitCommerTitle: string;
    brnStartDate: string;
    brnUnitPhone: string;
    brnUnitFax: string;
    brnDolDoyDoy: string;
};

export type BranchAbrRecord = {
    bacUnitNumber: string;
    bacActCode: string;
    bacActDescr: string;
    bacKind: '1' | '2' | '3' | '4';
    bacStartDate: string;
};

export type FirmAbroadRecord = {
    fabAfmAbroad: string;
    fabAppelationAbroad: string;
    fabAddressAbroad: string;
    fabCountry: string;
    fabStartDate: string;
    fabObjective: string;
    fabTaxAuthority: string;
    fabActCode: string;
    fabActDescr: string;
    code: string;
    description: string;
    callSequenceId: string;
    callSequenceDate: string;
};

export type RetrieveInfoByAFMRecordOutput = {
    basicInfo: BasicInfoRecord | null;
    firmActivitiesArray: FirmActivitiesRecord[] | null;
    membershipsArray: MembershipRecord[] | null;
    relationsArray: RelationRecord[] | null;
    symmetoxesArray: SymmetoxesRecord[] | null;
    sysxetiseisArray: SysxRecord[] | null;
    branchArray: BranchRecord[] | null;
    branchActivitiesArray: BranchAbrRecord[] | null;
    firmAbroad: FirmAbroadRecord | null;
};

export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};

export type RetrieveInfoByAFMInputRecord = {
    afm: string;
    firmActivitiesRequest?: boolean;
    membershipsRequest?: boolean;
    relationsRequest?: boolean;
    symmetoxesRequest?: boolean;
    sysxetiseisRequest?: boolean;
    branchActivitiesRequest?: boolean;
    branchAbroadRequest?: boolean;
    firmAbroadRequest?: boolean;
};

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};

/**
 *
 * @param input retrieveInfoByAFMInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export const getInfo = async (
    input: RetrieveInfoByAFMInputRecord,
    user: string,
    pass: string,
    overrides?: Overrides,
) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine =
        overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');

    const s = new SoapClient(wsdl, user, pass, auditRecord, endpoint);
    const response: RetrieveInfoByAFMRecordOutput | ErrorRecord =
        await s.getInfo(input);

    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};
export default getInfo;
