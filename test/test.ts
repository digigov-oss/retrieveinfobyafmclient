import getInfo from '../src/index';
import config from './config.json';
import inspect from 'object-inspect';

const test = async () => {
    // Try combinations to check the service in full.
    const inputs = [
        {
            afm: '010819637',
        },
        { 
            afm: '024176510'
        },
        { 
            afm: '108336710',
        },
        {
            afm: '979701876',
            firmActivitiesRequest: false,
        },
        {
            afm: '046253950',
            firmActivitiesRequest: true,
        },
    ];

    //loop on inputs array
    for (const input of inputs) {
        try {
            const response = await getInfo(input, config.user, config.pass);
            console.log(inspect(response, { depth: 10, indent: '\t' }));
        } catch (error) {
            console.log(error);
        }
    }
};

test();
