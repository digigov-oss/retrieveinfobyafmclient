import getInfo from '../dist/esm/index.js';
import config from './config.json';
import inspect from 'object-inspect';

const test = async () => {
    //Try compinations to check the service in full.
    const inputs = [
        {
            afm: '010819637',
        },
        {
            afm: '979701876',
            firmActivitiesRequest: false,
        },
        {
            afm: '046253950',
            firmActivitiesRequest: true,
        },
    ];
    //loop on inputs array
    for (const input of inputs) {
        try {
            const Info = await getInfo(input, config.user, config.pass);
            console.log(inspect(Info, { depth: 10, indent: '\t' }));
        } catch (error) {
            console.log(error);
        }
    }
};

test();
